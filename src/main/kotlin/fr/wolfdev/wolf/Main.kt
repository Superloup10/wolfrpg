package fr.wolfdev.wolf

import fr.wolfdev.engine.Game
import fr.wolfdev.engine.GameEngine
import fr.wolfdev.engine.GameInformation
import fr.wolfdev.engine.Version
import javax.swing.JButton
import javax.swing.JFrame

fun main(args: Array<String>) {
    if ("--waitRenderDoc" in args) {
        val frame = JFrame("Waiting for RenderDoc...")
        frame.add(
            JButton("Confirm that RenderDoc has been attached").apply {
                addActionListener {
                    frame.dispose()
                    start(true)
                }
            }
        )
        frame.pack()
        frame.setLocationRelativeTo(null)
        frame.isVisible = true
    } else {
        start(false)
    }
}

val wolfInfo = GameInformation("WolfRPG", Version(1, 0, 0))

fun start(renderDocUsed: Boolean) {
    GameEngine.launch(wolfInfo, object : Game() {}, renderDocUsed)
}
