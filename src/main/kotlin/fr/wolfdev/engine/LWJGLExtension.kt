package fr.wolfdev.engine

import org.lwjgl.PointerBuffer
import org.lwjgl.system.MemoryStack
import org.lwjgl.system.Pointer
import org.lwjgl.vulkan.EXTDebugReport.VK_ERROR_VALIDATION_FAILED_EXT
import org.lwjgl.vulkan.KHRDisplaySwapchain.VK_ERROR_INCOMPATIBLE_DISPLAY_KHR
import org.lwjgl.vulkan.KHRSurface.VK_ERROR_NATIVE_WINDOW_IN_USE_KHR
import org.lwjgl.vulkan.KHRSurface.VK_ERROR_SURFACE_LOST_KHR
import org.lwjgl.vulkan.KHRSwapchain.VK_ERROR_OUT_OF_DATE_KHR
import org.lwjgl.vulkan.KHRSwapchain.VK_SUBOPTIMAL_KHR
import org.lwjgl.vulkan.VK10.*
import java.nio.IntBuffer
import java.nio.LongBuffer

/**
 * Executes action between push and pop calls.
 */
inline fun <T> MemoryStack.useStack(action: MemoryStack.() -> T): T {
    push()
    val result = action()
    pop()
    return result
}

/**
 * Allow writing !ptr to emulate pointer dereferences
 */
operator fun PointerBuffer.not() = this[0]
operator fun Pointer.Default.not() = this.address()
operator fun LongBuffer.not() = this[0]
operator fun LongArray.not() = this[0]
operator fun IntArray.not() = this[0]
operator fun IntBuffer.not() = this[0]

operator fun LongBuffer.iterator() = elements().iterator()
operator fun PointerBuffer.iterator() = elements().iterator()
operator fun IntBuffer.iterator() = elements().iterator()

fun LongBuffer.elements(): Iterable<Long> = (0 until this.remaining()).map { this[it] }
fun PointerBuffer.elements(): Iterable<Long> = (0 until this.remaining()).map { this[it] }
fun IntBuffer.elements(): Iterable<Int> = (0 until this.remaining()).map { this[it] }

val Boolean.vk: Int get() = if (this) VK_TRUE else VK_FALSE

/**
 * Checks that this int is VK_SUCCESS or throws an exception with error message corresponding to this Vulkan error code
 */
fun Int.checkVKErrors() {
    if (isVkErrors) throw VulkanException(vkErrorToMessage())
}

val Int.isVkErrors: Boolean
    get() = this !in vkSuccessCodes

val vkSuccessCodes = intArrayOf(VK_SUCCESS, VK_NOT_READY, VK_TIMEOUT, VK_EVENT_SET, VK_EVENT_RESET, VK_INCOMPLETE, VK_SUBOPTIMAL_KHR)

fun Int.vkErrorToMessage(): String {
    val errorMessages = mapOf(
        // Success codes
        VK_SUCCESS to "Command successfully completed.",
        VK_NOT_READY to "A fence or query has not yet completed.",
        VK_TIMEOUT to "A wait operation has not completed in the specified time.",
        VK_EVENT_SET to "An event is signaled.",
        VK_EVENT_RESET to "An event is unsignaled.",
        VK_INCOMPLETE to "A return array was too small for the result.",
        VK_SUBOPTIMAL_KHR to "A swapchain no longer matches the surface properties exactly, but can still be used to present to the surface successfully.",

        // Error codes
        VK_ERROR_OUT_OF_HOST_MEMORY to "A host memory allocation has failed.",
        VK_ERROR_OUT_OF_DEVICE_MEMORY to "A device memory allocation has failed.",
        VK_ERROR_INITIALIZATION_FAILED to "Initialization of an object could not be completed for implementation-specific reasons.",
        VK_ERROR_DEVICE_LOST to "The logical or physical device has been lost.",
        VK_ERROR_MEMORY_MAP_FAILED to "Mapping of a memory object has failed.",
        VK_ERROR_LAYER_NOT_PRESENT to "A requested layer is not present or could not be loaded.",
        VK_ERROR_EXTENSION_NOT_PRESENT to "A requested extension is not supported.",
        VK_ERROR_FEATURE_NOT_PRESENT to "A requested feature is not supported.",
        VK_ERROR_INCOMPATIBLE_DRIVER to "The requested version of Vulkan is not supported by the driver or is otherwise incompatible for implementation-specific reasons.",
        VK_ERROR_TOO_MANY_OBJECTS to "Too many objects of the type have already been created.",
        VK_ERROR_FORMAT_NOT_SUPPORTED to "A requested format is not supported on this device.",
        VK_ERROR_SURFACE_LOST_KHR to "A surface is no longer available.",
        VK_ERROR_NATIVE_WINDOW_IN_USE_KHR to "The requested window is already connected to a VkSurfaceKHR, or to some other non-Vulkan API.",
        VK_ERROR_OUT_OF_DATE_KHR to "A surface has changed in such a way that it is no longer compatible with the swapchain, and further presentation requests using the swapchain will fail. Applications must query the new surface properties and recreate their swapchain if they wish to continue presenting to the surface.",
        VK_ERROR_INCOMPATIBLE_DISPLAY_KHR to "The display used by a swapchain does not use the same presentable image layout, or is incompatible in a way that prevents sharing an image.",
        VK_ERROR_VALIDATION_FAILED_EXT to "A validation layer found an error."
    )
    return errorMessages[this] ?: "Unknown Vulkan error [$this]"
}
