package fr.wolfdev.engine

class VulkanException(message: String) : RuntimeException(message)
