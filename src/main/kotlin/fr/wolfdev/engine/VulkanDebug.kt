package fr.wolfdev.engine

import fr.wolfdev.engine.render.VulkanRenderingEngine
import org.lwjgl.system.MemoryStack
import org.lwjgl.vulkan.*
import org.lwjgl.vulkan.EXTDebugReport.VK_DEBUG_REPORT_OBJECT_TYPE_UNKNOWN_EXT
import org.lwjgl.vulkan.EXTDebugUtils.*
import org.lwjgl.vulkan.VK10.VK_NULL_HANDLE

internal object VulkanDebug {
    internal var enabled = false
    private var debugger: VkDebugUtilsMessengerExt = VK_NULL_HANDLE
    private val stack = MemoryStack.create(64 * 1024) // 64 kb

    fun name(objectHandle: Long, name: String, type: Int = VK_DEBUG_REPORT_OBJECT_TYPE_UNKNOWN_EXT) {
        if (!enabled) return
        stack.useStack {
            val marker = VkDebugMarkerObjectNameInfoEXT.calloc(this).apply {
                sType(EXTDebugMarker.VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_NAME_INFO_EXT)
                pObjectName(UTF8(name))
                `object`(objectHandle)
                objectType(type)
            }

            EXTDebugMarker.vkDebugMarkerSetObjectNameEXT(VulkanRenderingEngine.logicalDevice, marker)
        }
    }

    internal fun init(vulkan: VkInstance, allocator: VkAllocationCallbacks?) {
        if (!vulkan.capabilities.VK_EXT_debug_utils) {
            throw VulkanException("Impossible to setup Vulkan logging, capability is not present")
        }
        stack.useStack {
            val createInfo = VkDebugUtilsMessengerCreateInfoEXT.calloc(this)
            populateDebugMessengerCreateInfo(createInfo)
            val ptr = mallocLong(1)
            vkCreateDebugUtilsMessengerEXT(vulkan, createInfo, allocator, ptr).checkVKErrors()
            debugger = !ptr
        }
    }

    internal fun populateDebugMessengerCreateInfo(createInfo: VkDebugUtilsMessengerCreateInfoEXT) {
        createInfo.apply {
            sType(VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT)
            pfnUserCallback { messageSeverity, messageTypes, pCallbackData, _ ->
                val message = VkDebugUtilsMessengerCallbackDataEXT.npMessageString(pCallbackData)
                val id = VkDebugUtilsMessengerCallbackDataEXT.npMessageIdNameString(pCallbackData) ?: "<>"
                val colorPrefix = when (messageSeverity) {
                    VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT -> "\u001b[33m"
                    VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT -> "\u001b[31m"
                    VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT -> "\u001b[34m"
                    else -> ""
                }
                val typePrefixes = mutableListOf<String>()
                if (messageTypes and VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT != 0) typePrefixes += "General"
                if (messageTypes and VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT != 0) typePrefixes += "Violation"
                if (messageTypes and VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT != 0) typePrefixes += "Performance"
                val reset = "\u001B[0m"
                println("$colorPrefix[Vulkan.${typePrefixes.joinToString(".")}] ($id) $message$reset")
                false.vk
            }
            messageSeverity(VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT or VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT or VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
            messageType(VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT or VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT or VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT)
        }
    }

    internal fun cleanUp(vulkan: VkInstance, allocator: VkAllocationCallbacks?) {
        vkDestroyDebugUtilsMessengerEXT(vulkan, debugger, allocator)
    }
}
