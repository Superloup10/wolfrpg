package fr.wolfdev.engine.math

import fr.wolfdev.engine.render.Vertex
import org.joml.*
import java.nio.Buffer
import java.nio.FloatBuffer
import kotlin.reflect.KClass
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.jvm.javaField

object SizeOfConfig {
    val sizes = mapOf(
        Boolean::class to 1,
        Byte::class to Byte.SIZE_BYTES,
        Short::class to Short.SIZE_BYTES,
        UShort::class to UShort.SIZE_BYTES,
        Float::class to Float.SIZE_BYTES,
        Int::class to Int.SIZE_BYTES,
        UInt::class to UInt.SIZE_BYTES,
        Char::class to Char.SIZE_BYTES,
        Long::class to Long.SIZE_BYTES,
        Double::class to Double.SIZE_BYTES,

        Matrix4f::class to 16 * Float.SIZE_BYTES,
        Matrix3f::class to 9 * Float.SIZE_BYTES,
        Vector4f::class to 4 * Float.SIZE_BYTES,
        Vector3f::class to 3 * Float.SIZE_BYTES,
        Vector2f::class to 2 * Float.SIZE_BYTES
    )
}

/**
 * Returns the size in bytes of a given type.
 *
 * This function returns the size used by the graphics card, not the JVM size.
 * Note: This function is specific to GPU communication and may not cover all cases exhaustively.
 */
inline fun <reified T> sizeof() = SizeOfConfig.sizes[T::class] ?: throw UnsupportedOperationException("Size of type ${T::class} is unknown")

/**
 * Calculates the byte offset of a specific property in a given class.
 *
 * @param clazz The class containing the property.
 * @param propertyName The name of the property for which the offset should be calculated.
 * @return The byte offset of the specified property in the given class.
 * @throws IllegalArgumentException If the property is not found in the class.
 * @throws UnsupportedOperationException If the property type is not supported.
 */
fun calculateOffset(clazz: KClass<*>, propertyName: String): Int {
    var offset = 0
    clazz.declaredMemberProperties.forEach {
        if (it.name == propertyName) {
            return offset
        }
        offset += when (it.returnType.classifier) {
            Float::class -> sizeof<Float>()
            Vector2f::class -> sizeof<Vector2f>()
            Vector3f::class -> sizeof<Vector3f>()
            Vector4f::class -> sizeof<Vector4f>()
            Matrix3f::class -> sizeof<Matrix3f>()
            Matrix4f::class -> sizeof<Matrix4f>()
            // Add more types as needed
            else -> throw UnsupportedOperationException("Unsupported property type")
        }
    }
    throw IllegalArgumentException("Property '$propertyName' not found in class '${clazz.simpleName}'")
}

fun FloatBuffer.put(vertex: Vertex) {
    val fields = vertex::class.declaredMemberProperties.map { it.javaField!! }
    fields.forEach { field ->
        field.isAccessible = true
        when (val value = field[vertex]) {
            is Float -> {
                put(value)
            }

            is Vector2f -> {
                put(value.x())
                put(value.y())
            }

            is Vector3f -> {
                put(value.x())
                put(value.y())
                put(value.z())
            }
        }
    }
}

fun Buffer.skip(amount: Int): Buffer {
    this.position(this.position() + amount)
    return this
}
