package fr.wolfdev.engine

import com.astedt.robin.util.concurrency.nursery.Nursery

fun <T> openNursery(scope: Nursery.() -> T): T = Nursery.open(scope)
