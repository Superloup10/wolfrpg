package fr.wolfdev.engine.render

import fr.wolfdev.engine.math.calculateOffset
import fr.wolfdev.engine.math.sizeof
import org.joml.Vector2f
import org.joml.Vector3f
import org.lwjgl.system.MemoryStack
import org.lwjgl.vulkan.VK10.*
import org.lwjgl.vulkan.VkVertexInputAttributeDescription
import org.lwjgl.vulkan.VkVertexInputBindingDescription

data class Vertex(val pos: Vector2f, val color: Vector3f) {
    companion object {
        internal val SIZE = sizeof<Vector2f>() + sizeof<Vector3f>()

        fun getBindingDescription(stack: MemoryStack): VkVertexInputBindingDescription.Buffer {
            val bindings = VkVertexInputBindingDescription.calloc(1, stack)
            bindings.binding(0)
            bindings.stride(SIZE)
            bindings.inputRate(VK_VERTEX_INPUT_RATE_VERTEX)
            return bindings
        }

        fun getAttributeDescription(stack: MemoryStack): VkVertexInputAttributeDescription.Buffer {
            val attributes = VkVertexInputAttributeDescription.calloc(2, stack)
            attributes[0].binding(0)
            attributes[0].location(0) // pos
            attributes[0].format(VK_FORMAT_R32G32_SFLOAT) // vec2f
            attributes[0].offset(calculateOffset(Vertex::class, "pos"))

            attributes[1].binding(0)
            attributes[1].location(1) // color
            attributes[1].format(VK_FORMAT_R32G32B32_SFLOAT) // vec3f
            attributes[1].offset(calculateOffset(Vertex::class, "color"))
            return attributes
        }
    }
}
