package fr.wolfdev.engine.render

import fr.wolfdev.engine.math.sizeof
import fr.wolfdev.engine.math.skip
import org.joml.Matrix4f
import java.nio.ByteBuffer

data class UniformBufferObject(
    val model: Matrix4f = Matrix4f(),
    val view: Matrix4f = Matrix4f(),
    val proj: Matrix4f = Matrix4f()
) {
    companion object {
        internal val SIZE = 3L * sizeof<Matrix4f>()
    }

    fun write(buffer: ByteBuffer): ByteBuffer {
        model.get(buffer)
        buffer.skip(sizeof<Matrix4f>())
        view.get(buffer)
        buffer.skip(sizeof<Matrix4f>())
        proj.get(buffer)
        return buffer.position(0)
    }
}
