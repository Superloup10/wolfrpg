package fr.wolfdev.engine.render

/**
 * Queues used by the rendering engine
 */
class QueueFamilies : Iterable<Int?> {
    var graphics: Int? = null
    var presents: Int? = null
    val isComplete get() = graphics != null && presents != null

    override fun iterator(): Iterator<Int?> {
        return listOf(graphics, presents).distinct().iterator()
    }
}
