package fr.wolfdev.engine.render

import fr.wolfdev.engine.GameInformation

/**
 * A Render Engine
 */
interface IRenderEngine {
    /**
     * Initialises the render engine
     */
    fun init(gameInfo: GameInformation, renderDocUsed: Boolean)
    fun loop()
    fun cleanUp()
}
