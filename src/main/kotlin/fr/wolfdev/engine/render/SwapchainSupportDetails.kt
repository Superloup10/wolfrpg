package fr.wolfdev.engine.render

import fr.wolfdev.engine.VkPresentModeKHR
import org.lwjgl.vulkan.VkSurfaceCapabilitiesKHR
import org.lwjgl.vulkan.VkSurfaceFormatKHR

/**
 * Represents what a swapchain can support
 */
class SwapchainSupportDetails {
    lateinit var capabilities: VkSurfaceCapabilitiesKHR
    lateinit var formats: VkSurfaceFormatKHR.Buffer
    lateinit var presentModes: VkPresentModeKHR
}
