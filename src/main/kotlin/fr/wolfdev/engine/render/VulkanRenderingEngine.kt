package fr.wolfdev.engine.render

import fr.wolfdev.engine.*
import fr.wolfdev.engine.VulkanDebug.name
import fr.wolfdev.engine.math.put
import fr.wolfdev.engine.math.sizeof
import org.joml.Math.clamp
import org.joml.Vector2f
import org.joml.Vector3f
import org.lwjgl.PointerBuffer
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.glfw.GLFWVulkan.glfwCreateWindowSurface
import org.lwjgl.glfw.GLFWVulkan.glfwGetRequiredInstanceExtensions
import org.lwjgl.system.Configuration.DEBUG
import org.lwjgl.system.MemoryStack
import org.lwjgl.system.MemoryUtil.*
import org.lwjgl.vulkan.*
import org.lwjgl.vulkan.EXTDebugReport.*
import org.lwjgl.vulkan.EXTDebugUtils.VK_EXT_DEBUG_UTILS_EXTENSION_NAME
import org.lwjgl.vulkan.KHRSurface.*
import org.lwjgl.vulkan.KHRSwapchain.*
import org.lwjgl.vulkan.VK10.*
import java.nio.ByteBuffer
import java.nio.IntBuffer
import java.nio.LongBuffer

object VulkanRenderingEngine : IRenderEngine {
    private const val ENGINE_NAME = "WolfEngine"
    private val ENGINE_VERSION = Version(0, 0, 0, "indev")
    private const val RENDER_WIDTH = 1920
    private const val RENDER_HEIGHT = 1080

    private val ENABLE_VALIDATION_LAYERS = DEBUG.get(true)
    private var windowPointer: Long = -1L
    private var framebufferResized = false
    private val validationLayers = if (ENABLE_VALIDATION_LAYERS) hashSetOf("VK_LAYER_KHRONOS_validation", "VK_LAYER_LUNARG_monitor") else null
    private val deviceExtensions = hashSetOf(VK_KHR_SWAPCHAIN_EXTENSION_NAME)
    private val renderDocExtensions = hashSetOf(EXTDebugMarker.VK_EXT_DEBUG_MARKER_EXTENSION_NAME)
    private val memoryStack = MemoryStack.create(512 * 1024 * 1024) // 512Mb
    private const val MAX_FRAME_IN_FLIGHT = 2

    private val vertices = listOf(
        Vertex(Vector2f(-0.5F, -0.5F), Vector3f(1.0F, 0.0F, 0.0F)), // bottom-left
        Vertex(Vector2f(0.5F, -0.5F), Vector3f(0.0F, 1.0F, 0.0F)), // bottom-right
        Vertex(Vector2f(0.5F, 0.5F), Vector3f(0.0F, 0.0F, 1.0F)), // top-right
        Vertex(Vector2f(-0.5F, 0.5F), Vector3f(1.0F, 1.0F, 1.0F)) // top-left
    )
    private val indices = listOf<UShort>(0u, 1u, 2u, 2u, 3u, 0u)

    // Start of Vulkan objects
    private val allocator: VkAllocationCallbacks? = null
    private lateinit var vulkan: VkInstance
    private var surface: VkSurfaceKHR = VK_NULL_HANDLE
    private var swapchain: VkSwapchainKHR = VK_NULL_HANDLE
    private lateinit var physicalDevice: VkPhysicalDevice
    internal lateinit var logicalDevice: VkDevice
    private lateinit var graphicsQueue: VkQueue
    private lateinit var presentsQueue: VkQueue
    private lateinit var swapChainImages: List<VkImage>
    private lateinit var swapChainImageViews: List<VkImageView>
    private var swapChainImageFormat: VkFormat = -1
    private lateinit var swapChainExtent: VkExtent2D
    private val dynamicStates = listOf(VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR)
    private var renderPass: VkRenderPass = VK_NULL_HANDLE
    private var descriptorSetLayout: VkDescriptorSetLayout = VK_NULL_HANDLE
    private var descriptorPool: VkDescriptorPool = VK_NULL_HANDLE
    private var pipelineLayout: VkPipelineLayout = VK_NULL_HANDLE
    private var graphicsPipeline: VkPipeline = VK_NULL_HANDLE
    private lateinit var swapChainFramebuffers: List<VkFramebuffer>
    private var commandPool: VkCommandPool = VK_NULL_HANDLE
    private var vertexBuffer: VkBuffer = VK_NULL_HANDLE
    private var vertexBufferMemory: VkDeviceMemory = VK_NULL_HANDLE
    private var indexBuffer: VkBuffer = VK_NULL_HANDLE
    private var indexBufferMemory: VkDeviceMemory = VK_NULL_HANDLE
    private lateinit var uniformBuffers: List<VkBuffer>
    private lateinit var uniformBuffersMemory: List<VkDeviceMemory>
    private lateinit var descriptorSets: List<VkDescriptorSet>
    private lateinit var commandBuffers: List<VkCommandBuffer>

    // Begin synchronisation
    private lateinit var imageAvailableSemaphores: List<VkSemaphore>
    private lateinit var renderFinishedSemaphores: List<VkSemaphore>
    private lateinit var inFlightFences: List<VkFence>
    private var currentFrame: Int = 0
    // End synchronisation
    // End of Vulkan Objects

    override fun init(gameInfo: GameInformation, renderDocUsed: Boolean) {
        VulkanDebug.enabled = renderDocUsed
        initWindow(gameInfo)
        createVulkanInstance(gameInfo)
        if (ENABLE_VALIDATION_LAYERS) {
            VulkanDebug.init(vulkan, allocator)
        }
        createSurface()
        pickPhysicalDevice()
        createLogicalDevice(renderDocUsed)
        createSwapchain()
        createImageViews()
        createRenderPass()
        createDescriptorSetLayout()
        createGraphicsPipeline()
        createFrameBuffers()
        createCommandPool()
        createVertexBuffer()
        createIndexBuffer()
        createUniformBuffers()
        createDescriptorPool()
        createDescriptorSets()
        createCommandBuffers()
        createSyncObjects()
    }

    private fun initWindow(gameInfo: GameInformation) {
        if (!glfwInit()) {
            throw IllegalStateException("Unable to initialize GLFW")
        }
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API)

        windowPointer = glfwCreateWindow(RENDER_WIDTH, RENDER_HEIGHT, gameInfo.name, NULL, NULL)
        glfwSetFramebufferSizeCallback(windowPointer) { _, _, _ ->
            framebufferResized = true
        }
    }

    private fun createVulkanInstance(gameInfo: GameInformation) {
        if (ENABLE_VALIDATION_LAYERS && !checkValidationLayerSupport()) {
            throw VulkanException("Validation requested but not supported")
        }
        useStack {
            val appInfo = VkApplicationInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_APPLICATION_INFO)
                apiVersion(VK_API_VERSION_1_0)
                pEngineName(UTF8(ENGINE_NAME))
                engineVersion(VK_MAKE_VERSION(ENGINE_VERSION.major, ENGINE_VERSION.minor, ENGINE_VERSION.patch))
                pApplicationName(UTF8(gameInfo.name))
                applicationVersion(VK_MAKE_VERSION(gameInfo.version.major, gameInfo.version.minor, gameInfo.version.patch))
            }

            val createInfo = VkInstanceCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO)
                pApplicationInfo(appInfo)
                ppEnabledExtensionNames(getRequiredExtensions(this@useStack))
                if (ENABLE_VALIDATION_LAYERS) {
                    ppEnabledLayerNames(validationLayers.asPointerBuffer(this@useStack))
                    val debugCreateInfo = VkDebugUtilsMessengerCreateInfoEXT.calloc(this@useStack)
                    VulkanDebug.populateDebugMessengerCreateInfo(debugCreateInfo)
                    pNext(debugCreateInfo.address())
                }
            }

            val vkPointer = mallocPointer(1)

            vkCreateInstance(createInfo, allocator, vkPointer).checkVKErrors()

            vulkan = VkInstance(!vkPointer, createInfo)
        }
    }

    private fun createSurface() {
        useStack {
            val pSurface = mallocLong(1)
            glfwCreateWindowSurface(vulkan, windowPointer, allocator, pSurface).checkVKErrors()
            surface = !pSurface
        }
    }

    private fun pickPhysicalDevice() {
        useStack {
            val deviceCount = ints(0)
            vkEnumeratePhysicalDevices(vulkan, deviceCount, null).checkVKErrors()
            if (!deviceCount == 0) {
                throw VulkanException("Failed to find GPUs with Vulkan support!")
            }
            val devices = mallocPointer(!deviceCount)
            vkEnumeratePhysicalDevices(vulkan, deviceCount, devices).checkVKErrors()

            physicalDevice = devices.elements()
                .map { VkPhysicalDevice(it, vulkan) }
                .firstOrNull(VulkanRenderingEngine::isValidDevice)
                ?: throw VulkanException("Could not find a suitable physical device!")
        }
    }

    private fun createLogicalDevice(renderDocUsed: Boolean) {
        useStack {
            val indices = findQueueFamilies(physicalDevice)
            val createQueueInfoBuffer = VkDeviceQueueCreateInfo.calloc(indices.asIterable().count(), this)
            for ((index, family) in indices.withIndex()) {
                createQueueInfoBuffer[index].apply {
                    sType(VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO)
                    queueFamilyIndex(family!!)
                    pQueuePriorities(floats(1.0F))
                }
            }
            val features = VkPhysicalDeviceFeatures.calloc(this)

            val createDeviceInfo = VkDeviceCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO)
                pQueueCreateInfos(createQueueInfoBuffer)
                pEnabledFeatures(features)
                val extensions = deviceExtensions + (if (renderDocUsed) renderDocExtensions else emptyList())
                ppEnabledExtensionNames(extensions.asPointerBuffer(this@useStack))
                if (ENABLE_VALIDATION_LAYERS) {
                    ppEnabledLayerNames(validationLayers.asPointerBuffer(this@useStack))
                }
            }

            val pDevice = mallocPointer(1)
            vkCreateDevice(physicalDevice, createDeviceInfo, allocator, pDevice).checkVKErrors()
            logicalDevice = VkDevice(!pDevice, physicalDevice, createDeviceInfo)
            name(!logicalDevice, "Logical Device", VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_EXT)

            val pQueue = mallocPointer(1)
            vkGetDeviceQueue(logicalDevice, indices.graphics!!, 0, pQueue)
            graphicsQueue = VkQueue(!pQueue, logicalDevice)
            name(!graphicsQueue, "Graphics Queue", VK_DEBUG_REPORT_OBJECT_TYPE_QUEUE_EXT)
            vkGetDeviceQueue(logicalDevice, indices.presents!!, 0, pQueue)
            presentsQueue = VkQueue(!pQueue, logicalDevice)
            name(!presentsQueue, "Presents Queue", VK_DEBUG_REPORT_OBJECT_TYPE_QUEUE_EXT)
        }
    }

    private fun createSwapchain() {
        useStack {
            val swapchainSupportDetails = querySwapChainSupport(physicalDevice, this)
            val surfaceFormat = chooseSwapSurfaceFormat(swapchainSupportDetails.formats)
            val presentMode = chooseSwapPresentMode(swapchainSupportDetails.presentModes)
            val extent = chooseSwapExtent(swapchainSupportDetails.capabilities, this)

            val imageCount = ints(swapchainSupportDetails.capabilities.minImageCount() + 1)
            if (swapchainSupportDetails.capabilities.maxImageCount() in 1 until !imageCount) {
                imageCount.put(0, swapchainSupportDetails.capabilities.maxImageCount())
            }

            val createInfo = VkSwapchainCreateInfoKHR.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR)
                surface(surface)

                // Image settings
                minImageCount(!imageCount)
                imageFormat(surfaceFormat.format())
                imageColorSpace(surfaceFormat.colorSpace())
                imageExtent(extent)
                imageArrayLayers(1)
                imageUsage(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT)

                val indices = findQueueFamilies(physicalDevice)
                if (indices.graphics != indices.presents) {
                    imageSharingMode(VK_SHARING_MODE_CONCURRENT)
                    pQueueFamilyIndices(ints(indices.graphics!!, indices.presents!!))
                    queueFamilyIndexCount(2)
                } else {
                    imageSharingMode(VK_SHARING_MODE_EXCLUSIVE)
                }
                preTransform(swapchainSupportDetails.capabilities.currentTransform())
                compositeAlpha(VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR)
                presentMode(presentMode)
                clipped(true)
                oldSwapchain(VK_NULL_HANDLE) // TODO: use for window resizing
            }

            val pSwapchain = longs(VK_NULL_HANDLE)
            vkCreateSwapchainKHR(logicalDevice, createInfo, allocator, pSwapchain).checkVKErrors()
            swapchain = !pSwapchain
            name(swapchain, "Swap Chain", VK_DEBUG_REPORT_OBJECT_TYPE_SWAPCHAIN_KHR_EXT)

            vkGetSwapchainImagesKHR(logicalDevice, swapchain, imageCount, null)
            val pImages = mallocLong(!imageCount)
            vkGetSwapchainImagesKHR(logicalDevice, swapchain, imageCount, pImages)
            swapChainImages = pImages.elements().toList()
            swapChainImages.forEachIndexed { index, image -> name(image, "Swap Chain Image n°$index", VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_EXT) }
            swapChainImageFormat = surfaceFormat.format()
            swapChainExtent = VkExtent2D.create().set(extent)
        }
    }

    private fun createImageViews() {
        useStack {
            val imageViews = mutableListOf<VkImageView>()
            for ((index, image) in swapChainImages.withIndex()) {
                val createInfo = VkImageViewCreateInfo.calloc(this).apply {
                    sType(VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO)
                    image(image)
                    viewType(VK_IMAGE_VIEW_TYPE_2D)
                    format(swapChainImageFormat)
                    components().apply {
                        r(VK_COMPONENT_SWIZZLE_IDENTITY)
                        g(VK_COMPONENT_SWIZZLE_IDENTITY)
                        b(VK_COMPONENT_SWIZZLE_IDENTITY)
                        a(VK_COMPONENT_SWIZZLE_IDENTITY)
                    }
                    subresourceRange().apply {
                        aspectMask(VK_IMAGE_ASPECT_COLOR_BIT)
                        // no mipmapping
                        baseMipLevel(0)
                        levelCount(1)
                        baseArrayLayer(0)
                        layerCount(1)
                    }
                }
                val pView = mallocLong(1)
                vkCreateImageView(logicalDevice, createInfo, allocator, pView).checkVKErrors()
                name(!pView, "Render Image View #$index", VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_VIEW_EXT)
                imageViews += !pView
            }
            swapChainImageViews = imageViews
        }
    }

    private fun createRenderPass() {
        useStack {
            val colorAttachments = VkAttachmentDescription.calloc(1, this)
            colorAttachments.format(swapChainImageFormat)
            colorAttachments.samples(VK_SAMPLE_COUNT_1_BIT)
            colorAttachments.loadOp(VK_ATTACHMENT_LOAD_OP_CLEAR)
            colorAttachments.storeOp(VK_ATTACHMENT_STORE_OP_STORE)
            colorAttachments.stencilLoadOp(VK_ATTACHMENT_LOAD_OP_DONT_CARE)
            colorAttachments.stencilStoreOp(VK_ATTACHMENT_STORE_OP_DONT_CARE)
            colorAttachments.initialLayout(VK_IMAGE_LAYOUT_UNDEFINED)
            colorAttachments.finalLayout(VK_IMAGE_LAYOUT_PRESENT_SRC_KHR)

            val colorAttachmentRef = VkAttachmentReference.calloc(1, this)
            colorAttachmentRef.attachment(0)
            colorAttachmentRef.layout(VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)

            val subpass = VkSubpassDescription.calloc(1, this)
            subpass.pipelineBindPoint(VK_PIPELINE_BIND_POINT_GRAPHICS)
            subpass.colorAttachmentCount(1)
            subpass.pColorAttachments(colorAttachmentRef)

            val dependencies = VkSubpassDependency.calloc(1, this)
            dependencies.srcSubpass(VK_SUBPASS_EXTERNAL)
            dependencies.dstSubpass(0)
            dependencies.srcStageMask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT)
            dependencies.srcAccessMask(0)
            dependencies.dstStageMask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT)
            dependencies.dstAccessMask(VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT)

            val renderPassInfo = VkRenderPassCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO)
                pAttachments(colorAttachments)
                pSubpasses(subpass)
                pDependencies(dependencies)
            }
            val pRenderPass = mallocLong(1)
            vkCreateRenderPass(logicalDevice, renderPassInfo, allocator, pRenderPass).checkVKErrors()
            renderPass = !pRenderPass
            name(renderPass, "RenderPass", VK_DEBUG_REPORT_OBJECT_TYPE_RENDER_PASS_EXT)
        }
    }

    private fun createDescriptorSetLayout() {
        useStack {
            val uboLayoutBinding = VkDescriptorSetLayoutBinding.calloc(1, this)
            uboLayoutBinding.binding(0)
            uboLayoutBinding.descriptorType(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER)
            uboLayoutBinding.descriptorCount(1)
            uboLayoutBinding.stageFlags(VK_SHADER_STAGE_VERTEX_BIT)

            val layoutInfo = VkDescriptorSetLayoutCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO)
                pBindings(uboLayoutBinding)
            }

            val pDescriptor = mallocLong(1)
            vkCreateDescriptorSetLayout(logicalDevice, layoutInfo, allocator, pDescriptor).checkVKErrors()
            descriptorSetLayout = !pDescriptor
            name(descriptorSetLayout, "Descriptor set layout", VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT_EXT)
        }
    }

    private fun createGraphicsPipeline() {
        val vertCode = javaClass.getResourceAsStream("/shaders/vert.spv")!!.readBytes()
        val fragCode = javaClass.getResourceAsStream("/shaders/frag.spv")!!.readBytes()

        val vertShaderModule = createShaderModule(vertCode)
        name(vertShaderModule, "Vertex Shader Module", VK_DEBUG_REPORT_OBJECT_TYPE_SHADER_MODULE_EXT)
        val fragShaderModule = createShaderModule(fragCode)
        name(fragShaderModule, "Fragment Shader Module", VK_DEBUG_REPORT_OBJECT_TYPE_SHADER_MODULE_EXT)

        useStack {
            val vertShaderStageInfo = VkPipelineShaderStageCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO)
                stage(VK_SHADER_STAGE_VERTEX_BIT)
                module(vertShaderModule)
                pName(UTF8("main"))
            }

            val fragShaderStageInfo = VkPipelineShaderStageCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO)
                stage(VK_SHADER_STAGE_FRAGMENT_BIT)
                module(fragShaderModule)
                pName(UTF8("main"))
            }

            val shadersStage = VkPipelineShaderStageCreateInfo.calloc(2, this)
            shadersStage.put(vertShaderStageInfo)
            shadersStage.put(fragShaderStageInfo)
            shadersStage.flip()

            val vertexInputState = VkPipelineVertexInputStateCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO)
                pVertexBindingDescriptions(Vertex.getBindingDescription(this@useStack))
                pVertexAttributeDescriptions(Vertex.getAttributeDescription(this@useStack))
            }

            val inputAssemblyState = VkPipelineInputAssemblyStateCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO)
                topology(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST)
                primitiveRestartEnable(false)
            }

            val viewportState = VkPipelineViewportStateCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO)
                viewportCount(1)
                scissorCount(1)
            }

            val rasterizerState = VkPipelineRasterizationStateCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO)
                depthClampEnable(false)
                rasterizerDiscardEnable(false)
                polygonMode(VK_POLYGON_MODE_FILL)
                lineWidth(1.0F)
                cullMode(VK_CULL_MODE_BACK_BIT)
                frontFace(VK_FRONT_FACE_COUNTER_CLOCKWISE)
                depthBiasEnable(false)
            }

            val multisamplingState = VkPipelineMultisampleStateCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO)
                sampleShadingEnable(false)
                rasterizationSamples(VK_SAMPLE_COUNT_1_BIT)
            }

            val colorBlendAttachments = VkPipelineColorBlendAttachmentState.calloc(1, this)
            colorBlendAttachments.colorWriteMask(VK_COLOR_COMPONENT_R_BIT or VK_COLOR_COMPONENT_G_BIT or VK_COLOR_COMPONENT_B_BIT or VK_COLOR_COMPONENT_A_BIT)
            colorBlendAttachments.blendEnable(false)

            val colorBlendingState = VkPipelineColorBlendStateCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO)
                logicOpEnable(false)
                pAttachments(colorBlendAttachments)
            }

            val dynamicState = VkPipelineDynamicStateCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO)
                pDynamicStates(dynamicStates.asIntBuffer(this@useStack))
            }

            val pipelineLayoutInfo = VkPipelineLayoutCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO)
                pSetLayouts(longs(descriptorSetLayout))
            }

            val pPipeline = longs(VK_NULL_HANDLE)
            vkCreatePipelineLayout(logicalDevice, pipelineLayoutInfo, allocator, pPipeline).checkVKErrors()
            pipelineLayout = !pPipeline
            name(pipelineLayout, "PipelineLayout($vertShaderModule, $fragShaderModule)", VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_LAYOUT_EXT)

            val pipelineInfo = VkGraphicsPipelineCreateInfo.calloc(1, this)
            pipelineInfo.sType(VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO)
            pipelineInfo.pStages(shadersStage)
            pipelineInfo.pVertexInputState(vertexInputState)
            pipelineInfo.pInputAssemblyState(inputAssemblyState)
            pipelineInfo.pViewportState(viewportState)
            pipelineInfo.pRasterizationState(rasterizerState)
            pipelineInfo.pMultisampleState(multisamplingState)
            pipelineInfo.pColorBlendState(colorBlendingState)
            pipelineInfo.pDynamicState(dynamicState)
            pipelineInfo.layout(pipelineLayout)
            pipelineInfo.renderPass(renderPass)
            pipelineInfo.subpass(0)

            val pGraphicsPipeline = mallocLong(1)
            vkCreateGraphicsPipelines(logicalDevice, VK_NULL_HANDLE, pipelineInfo, allocator, pGraphicsPipeline).checkVKErrors()
            graphicsPipeline = !pGraphicsPipeline
            name(graphicsPipeline, "GraphicsPipeline($vertShaderModule, $fragShaderModule)", VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_EXT)
        }

        vkDestroyShaderModule(logicalDevice, vertShaderModule, allocator)
        vkDestroyShaderModule(logicalDevice, fragShaderModule, allocator)
    }

    private fun createFrameBuffers() {
        swapChainFramebuffers = ArrayList(swapChainImageViews.size)
        useStack {
            swapChainImageViews.forEach {
                val attachment = mallocLong(1).apply {
                    put(it)
                    rewind()
                }

                val framebufferCreateInfo = VkFramebufferCreateInfo.calloc(this).apply {
                    sType(VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO)
                    renderPass(renderPass)
                    pAttachments(attachment)
                    width(swapChainExtent.width())
                    height(swapChainExtent.height())
                    layers(1)
                }

                val pFramebuffer = mallocLong(1)
                vkCreateFramebuffer(logicalDevice, framebufferCreateInfo, allocator, pFramebuffer).checkVKErrors()
                name(!pFramebuffer, "Main framebuffer #$frameIndex", VK_DEBUG_REPORT_OBJECT_TYPE_FRAMEBUFFER_EXT)
                swapChainFramebuffers += !pFramebuffer
            }
        }
    }

    private fun createCommandPool() {
        useStack {
            val families = findQueueFamilies(physicalDevice)

            val poolInfo = VkCommandPoolCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO)
                flags(VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
                queueFamilyIndex(families.graphics!!)
            }

            val pCommandPool = mallocLong(1)
            vkCreateCommandPool(logicalDevice, poolInfo, allocator, pCommandPool).checkVKErrors()
            commandPool = !pCommandPool
            name(commandPool, "Command pool", VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_POOL_EXT)
        }
    }

    private fun createVertexBuffer() {
        useStack {
            val bufferSize = (vertices.size * Vertex.SIZE).toLong()
            val pBuffer = mallocLong(1)
            val pBufferMemory = mallocLong(1)
            createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT or VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, pBuffer, pBufferMemory)
            val stagingBuffer = !pBuffer
            val stagingBufferMemory = !pBufferMemory

            val pData = mallocPointer(1)
            vkMapMemory(logicalDevice, stagingBufferMemory, 0, bufferSize, 0, pData)
            val buffer = malloc(bufferSize.toInt())
            val fb = buffer.asFloatBuffer()
            for (vertex in vertices) {
                fb.put(vertex)
            }
            memCopy(memAddress(buffer), !pData, buffer.remaining().toLong())
            vkUnmapMemory(logicalDevice, stagingBufferMemory)

            createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT or VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, pBuffer, pBufferMemory)

            vertexBuffer = !pBuffer
            name(vertexBuffer, "Vertex Buffer", VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT)

            vertexBufferMemory = !pBufferMemory
            name(vertexBufferMemory, "Vertex Buffer Memory", VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_MEMORY_EXT)

            copyBuffer(stagingBuffer, vertexBuffer, bufferSize)

            vkDestroyBuffer(logicalDevice, stagingBuffer, allocator)
            vkFreeMemory(logicalDevice, stagingBufferMemory, allocator)
        }
    }

    private fun createIndexBuffer() {
        useStack {
            val bufferSize = (indices.size * sizeof<UShort>()).toLong()
            val pBuffer = mallocLong(1)
            val pBufferMemory = mallocLong(1)
            createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT or VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, pBuffer, pBufferMemory)
            val stagingBuffer = !pBuffer
            val stagingBufferMemory = !pBufferMemory

            val pData = mallocPointer(1)
            vkMapMemory(logicalDevice, stagingBufferMemory, 0, bufferSize, 0, pData)
            val buffer = malloc(bufferSize.toInt())
            val sb = buffer.asShortBuffer()
            for (index in indices) {
                sb.put(index.toShort())
            }
            memCopy(memAddress(buffer), !pData, buffer.remaining().toLong())
            vkUnmapMemory(logicalDevice, stagingBufferMemory)

            createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT or VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, pBuffer, pBufferMemory)

            indexBuffer = !pBuffer
            name(indexBuffer, "Index Buffer", VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT)

            indexBufferMemory = !pBufferMemory
            name(indexBufferMemory, "Index Buffer Memory", VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_MEMORY_EXT)

            copyBuffer(stagingBuffer, indexBuffer, bufferSize)

            vkDestroyBuffer(logicalDevice, stagingBuffer, allocator)
            vkFreeMemory(logicalDevice, stagingBufferMemory, allocator)
        }
    }

    private fun createUniformBuffers() {
        useStack {
            uniformBuffers = ArrayList(MAX_FRAME_IN_FLIGHT)
            uniformBuffersMemory = ArrayList(MAX_FRAME_IN_FLIGHT)
            for (i in 0 until MAX_FRAME_IN_FLIGHT) {
                val pBuffer = mallocLong(1)
                val pBufferMemory = mallocLong(1)
                createBuffer(UniformBufferObject.SIZE, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT or VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, pBuffer, pBufferMemory)
                name(!pBuffer, "Uniform Buffer n°$i", VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT)
                uniformBuffers += !pBuffer
                name(!pBufferMemory, "Uniform Buffer Memory n°$i", VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_MEMORY_EXT)
                uniformBuffersMemory += !pBufferMemory
            }
        }
    }

    private fun createDescriptorPool() {
        useStack {
            val poolSize = VkDescriptorPoolSize.calloc(1, this)
            poolSize.type(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER)
            poolSize.descriptorCount(MAX_FRAME_IN_FLIGHT)

            val poolInfo = VkDescriptorPoolCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO)
                pPoolSizes(poolSize)
                maxSets(MAX_FRAME_IN_FLIGHT)
            }

            val pPool = mallocLong(1)
            vkCreateDescriptorPool(logicalDevice, poolInfo, allocator, pPool).checkVKErrors()
            descriptorPool = !pPool
            name(descriptorPool, "Descriptor Pool", VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_POOL_EXT)
        }
    }

    private fun createDescriptorSets() {
        useStack {
            val layouts = mallocLong(MAX_FRAME_IN_FLIGHT)
            for (i in 0 until layouts.capacity()) {
                layouts.put(i, descriptorSetLayout)
            }
            val allocInfo = VkDescriptorSetAllocateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO)
                descriptorPool(descriptorPool)
                pSetLayouts(layouts)
            }

            val pSets = mallocLong(MAX_FRAME_IN_FLIGHT)
            vkAllocateDescriptorSets(logicalDevice, allocInfo, pSets).checkVKErrors()

            descriptorSets = ArrayList(pSets.capacity())

            for (i in 0 until pSets.capacity()) {
                val bufferInfo = VkDescriptorBufferInfo.calloc(1, this)
                bufferInfo.buffer(uniformBuffers[i])
                bufferInfo.offset(0)
                bufferInfo.range(UniformBufferObject.SIZE)

                val descriptorWrite = VkWriteDescriptorSet.calloc(1, this)
                descriptorWrite.sType(VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET)
                descriptorWrite.dstSet(pSets[i])
                descriptorWrite.dstBinding(0)
                descriptorWrite.dstArrayElement(0)
                descriptorWrite.descriptorType(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER)
                descriptorWrite.descriptorCount(1)
                descriptorWrite.pBufferInfo(bufferInfo)

                vkUpdateDescriptorSets(logicalDevice, descriptorWrite, null)
                descriptorSets += pSets[i]
                name(!pSets, "Descriptor Set n°$i", VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_EXT)
            }
        }
    }

    private fun copyBuffer(srcBuffer: VkBuffer, dstBuffer: VkBuffer, size: VkDeviceSize) {
        useStack {
            val allocInfo = VkCommandBufferAllocateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO)
                level(VK_COMMAND_BUFFER_LEVEL_PRIMARY)
                commandPool(commandPool)
                commandBufferCount(1)
            }

            val pBuffer = mallocPointer(1)
            vkAllocateCommandBuffers(logicalDevice, allocInfo, pBuffer).checkVKErrors()
            val commandBuffer = VkCommandBuffer(!pBuffer, logicalDevice)

            val beginInfo = VkCommandBufferBeginInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO)
                flags(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT)
            }

            vkBeginCommandBuffer(commandBuffer, beginInfo).checkVKErrors()

            val copyRegion = VkBufferCopy.calloc(1, this)
            copyRegion.size(size)
            vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, copyRegion)

            vkEndCommandBuffer(commandBuffer).checkVKErrors()

            val submitInfo = VkSubmitInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_SUBMIT_INFO)
                pCommandBuffers(pBuffer)
            }
            vkQueueSubmit(graphicsQueue, submitInfo, VK_NULL_HANDLE)
            vkQueueWaitIdle(graphicsQueue)
            vkFreeCommandBuffers(logicalDevice, commandPool, pBuffer)
        }
    }

    private fun createBuffer(size: VkDeviceSize, usage: VkBufferUsageFlags, properties: VkMemoryPropertyFlags, buffer: LongBuffer, bufferMemory: LongBuffer) {
        useStack {
            val bufferInfo = VkBufferCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO)
                size(size)
                usage(usage)
                sharingMode(VK_SHARING_MODE_EXCLUSIVE)
            }

            vkCreateBuffer(logicalDevice, bufferInfo, allocator, buffer).checkVKErrors()

            val memoryRequirements = VkMemoryRequirements.calloc(this)
            vkGetBufferMemoryRequirements(logicalDevice, !buffer, memoryRequirements)

            val allocInfo = VkMemoryAllocateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO)
                allocationSize(memoryRequirements.size())
                memoryTypeIndex(findMemoryType(memoryRequirements.memoryTypeBits(), properties))
            }

            vkAllocateMemory(logicalDevice, allocInfo, allocator, bufferMemory).checkVKErrors()

            vkBindBufferMemory(logicalDevice, !buffer, !bufferMemory, 0)
        }
    }

    private fun createCommandBuffers() {
        useStack {
            val allocInfo = VkCommandBufferAllocateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO)
                commandPool(commandPool)
                level(VK_COMMAND_BUFFER_LEVEL_PRIMARY)
                commandBufferCount(MAX_FRAME_IN_FLIGHT)
            }

            val pCommandBuffers = mallocPointer(MAX_FRAME_IN_FLIGHT)
            vkAllocateCommandBuffers(logicalDevice, allocInfo, pCommandBuffers).checkVKErrors()

            commandBuffers = pCommandBuffers.elements().map { VkCommandBuffer(it, logicalDevice) }
            commandBuffers.forEachIndexed { index, commandBuffer ->
                name(!commandBuffer, "Command Buffer n°$index", VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_BUFFER_EXT)
            }
        }
    }

    private fun recordCommandBuffer(commandBuffer: VkCommandBuffer, imageIndex: Int) {
        useStack {
            val beginInfo = VkCommandBufferBeginInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO)
            }

            vkBeginCommandBuffer(commandBuffer, beginInfo).checkVKErrors()

            val renderPassInfo = VkRenderPassBeginInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO)
                renderPass(renderPass)
                framebuffer(swapChainFramebuffers[imageIndex])
                renderArea().apply {
                    offset(VkOffset2D.calloc(this@useStack).set(0, 0))
                    extent(swapChainExtent)
                }
                val clearValue = VkClearValue.calloc(1, this@useStack)
                clearValue.color().float32(floats(0.0F, 0.0F, 0.0F, 0.0F))
                pClearValues(clearValue)
            }

            vkCmdBeginRenderPass(commandBuffer, renderPassInfo, VK_SUBPASS_CONTENTS_INLINE)

            vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline)

            val viewport = VkViewport.calloc(1, this)
            viewport.x(0.0F)
            viewport.y(0.0F)
            viewport.width(swapChainExtent.width().toFloat())
            viewport.height(swapChainExtent.height().toFloat())
            viewport.minDepth(0.0F)
            viewport.maxDepth(1.0F)
            vkCmdSetViewport(commandBuffer, 0, viewport)

            val scissor = VkRect2D.calloc(1, this)
            scissor.offset(VkOffset2D.calloc(this@useStack).set(0, 0))
            scissor.extent(swapChainExtent)
            vkCmdSetScissor(commandBuffer, 0, scissor)

            val pVertexBuffer = longs(vertexBuffer)
            val pOffsets = longs(0)
            vkCmdBindVertexBuffers(commandBuffer, 0, pVertexBuffer, pOffsets)
            vkCmdBindIndexBuffer(commandBuffer, indexBuffer, 0, VK_INDEX_TYPE_UINT16)
            val pSets = longs(descriptorSets[currentFrame])
            vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, pSets, null)

            vkCmdDrawIndexed(commandBuffer, indices.size, 1, 0, 0, 0)

            vkCmdEndRenderPass(commandBuffer)

            vkEndCommandBuffer(commandBuffer).checkVKErrors()
        }
    }

    private fun createSyncObjects() {
        imageAvailableSemaphores = ArrayList(MAX_FRAME_IN_FLIGHT)
        renderFinishedSemaphores = ArrayList(MAX_FRAME_IN_FLIGHT)
        inFlightFences = ArrayList(MAX_FRAME_IN_FLIGHT)
        useStack {
            val semaphoreInfo = VkSemaphoreCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO)
            }

            val fenceInfo = VkFenceCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_FENCE_CREATE_INFO)
                flags(VK_FENCE_CREATE_SIGNALED_BIT)
            }

            val pImageAvailableSemaphore = mallocLong(1)
            val pRenderFinishedSemaphore = mallocLong(1)
            val pFence = mallocLong(1)

            for (i in 0 until MAX_FRAME_IN_FLIGHT) {
                vkCreateSemaphore(logicalDevice, semaphoreInfo, allocator, pImageAvailableSemaphore).checkVKErrors()
                name(!pImageAvailableSemaphore, "Image available Semaphore n°$i", VK_DEBUG_REPORT_OBJECT_TYPE_SEMAPHORE_EXT)
                imageAvailableSemaphores += !pImageAvailableSemaphore

                vkCreateSemaphore(logicalDevice, semaphoreInfo, allocator, pRenderFinishedSemaphore).checkVKErrors()
                name(!pRenderFinishedSemaphore, "Render finished Semaphore n°$i", VK_DEBUG_REPORT_OBJECT_TYPE_SEMAPHORE_EXT)
                renderFinishedSemaphores += !pRenderFinishedSemaphore

                vkCreateFence(logicalDevice, fenceInfo, allocator, pFence).checkVKErrors()
                name(!pFence, "Fence n°$i", VK_DEBUG_REPORT_OBJECT_TYPE_FENCE_EXT)
                inFlightFences += !pFence
            }
        }
    }

    private fun drawFrame() {
        vkWaitForFences(logicalDevice, inFlightFences[currentFrame], true, Long.MAX_VALUE)

        useStack {
            val pImageIndex = mallocInt(1)
            var result = vkAcquireNextImageKHR(logicalDevice, swapchain, Long.MAX_VALUE, imageAvailableSemaphores[currentFrame], VK_NULL_HANDLE, pImageIndex)
            if (result == VK_ERROR_OUT_OF_DATE_KHR) { // swapchain is no longer adequate, update
                recreateSwapChain()
                return@useStack
            } else {
                result.checkVKErrors()
            }

            vkResetFences(logicalDevice, inFlightFences[currentFrame])

            vkResetCommandBuffer(commandBuffers[currentFrame], 0)
            recordCommandBuffer(commandBuffers[currentFrame], !pImageIndex)

            updateUniformBuffer(currentFrame)

            val waitSemaphores = longs(imageAvailableSemaphores[currentFrame])
            val waitStages = ints(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT)
            val pCommandBuffers = pointers(commandBuffers[currentFrame])
            val signalSemaphores = longs(renderFinishedSemaphores[currentFrame])
            val submitInfo = VkSubmitInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_SUBMIT_INFO)
                waitSemaphoreCount(1)
                pWaitSemaphores(waitSemaphores)
                pWaitDstStageMask(waitStages)
                pCommandBuffers(pCommandBuffers)
                pSignalSemaphores(signalSemaphores)
            }

            result = vkQueueSubmit(graphicsQueue, submitInfo, inFlightFences[currentFrame])
            if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || framebufferResized) {
                framebufferResized = false
                recreateSwapChain()
                return@useStack
            } else {
                result.checkVKErrors()
            }

            val presentInfo = VkPresentInfoKHR.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_PRESENT_INFO_KHR)
                pWaitSemaphores(signalSemaphores)
                val pSwapchain = longs(swapchain)
                swapchainCount(1)
                pSwapchains(pSwapchain)
                pImageIndices(pImageIndex)
            }
            vkQueuePresentKHR(presentsQueue, presentInfo)
            currentFrame = (currentFrame + 1) % MAX_FRAME_IN_FLIGHT
        }
    }

    private fun updateUniformBuffer(currentImage: Int) {
        val time = glfwGetTime()
        val ubo = UniformBufferObject().apply {
            model.identity().rotate((time * Math.toRadians(90.0)).toFloat(), Vector3f(0.0F, 0.0F, 1.0F))
            view.identity().lookAt(Vector3f(2.0f), Vector3f(), Vector3f(0.0F, 0.0F, 1.0F))
            proj.identity().perspective(Math.toRadians(45.0).toFloat(), (swapChainExtent.width() / swapChainExtent.height()).toFloat(), 0.1F, 10.0F)
            proj.m11(proj.m11() * -1) // invert Y Axis (OpenGL -> Vulkan translation)
        }
        useStack {
            val pData = mallocPointer(1)
            vkMapMemory(logicalDevice, uniformBuffersMemory[currentImage], 0, UniformBufferObject.SIZE, 0, pData)
            val buffer = ubo.write(malloc(UniformBufferObject.SIZE.toInt()))
            memCopy(memAddress(buffer), !pData, buffer.remaining().toLong())
            vkUnmapMemory(logicalDevice, uniformBuffersMemory[currentImage])
        }
    }

    private fun recreateSwapChain() {
        useStack {
            val width = ints(0)
            val height = ints(0)
            glfwGetFramebufferSize(windowPointer, width, height)
            while (!width == 0 || !height == 0) {
                glfwGetFramebufferSize(windowPointer, width, height)
                glfwWaitEvents()
            }
        }
        vkDeviceWaitIdle(logicalDevice)
        cleanUpSwapChain()
        createSwapchain()
        createImageViews()
        createFrameBuffers()
    }

    private fun cleanUpSwapChain() {
        swapChainFramebuffers.forEach { vkDestroyFramebuffer(logicalDevice, it, allocator) }
        swapChainImageViews.forEach { vkDestroyImageView(logicalDevice, it, allocator) }
        vkDestroySwapchainKHR(logicalDevice, swapchain, allocator)
    }

    private fun createShaderModule(spirvCode: ByteArray): VkShaderModule {
        return useStack {
            val createInfo = VkShaderModuleCreateInfo.calloc(this).apply {
                sType(VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO)
                val codeBuffer = malloc(spirvCode.size).apply {
                    put(spirvCode)
                    rewind()
                }
                pCode(codeBuffer)
            }
            val pShader = mallocLong(1)
            vkCreateShaderModule(logicalDevice, createInfo, allocator, pShader).checkVKErrors()
            !pShader
        }
    }

    private fun findMemoryType(typeFilter: Int, properties: VkMemoryPropertyFlags): Int {
        return useStack {
            val memProperties = VkPhysicalDeviceMemoryProperties.calloc(this)
            vkGetPhysicalDeviceMemoryProperties(physicalDevice, memProperties)
            for (i in 0 until memProperties.memoryTypeCount()) {
                if (typeFilter and (1 shl i) != 0 && (memProperties.memoryTypes(i).propertyFlags() and properties) == properties) {
                    return@useStack i
                }
            }
            throw VulkanException("Failed to find suitable memory type")
        }
    }

    private fun Collection<Int>.asIntBuffer(stack: MemoryStack): IntBuffer {
        val buffer = stack.mallocInt(this.size)
        this.forEach(buffer::put)
        return buffer.rewind()
    }

    private fun Collection<String>?.asPointerBuffer(stack: MemoryStack): PointerBuffer {
        val buffer = stack.mallocPointer(this!!.size)
        this.map(stack::UTF8).forEach(buffer::put)
        return buffer.rewind()
    }

    private fun checkExtensions(device: VkPhysicalDevice): Boolean {
        return useStack {
            val pCount = ints(0)
            vkEnumerateDeviceExtensionProperties(device, null as ByteBuffer?, pCount, null)
            val properties = VkExtensionProperties.calloc(!pCount, this)
            vkEnumerateDeviceExtensionProperties(device, null as ByteBuffer?, pCount, properties)
            val availableExtensions = properties.map { it.extensionNameString() }
            deviceExtensions.all { it in availableExtensions }
        }
    }

    private fun getRequiredExtensions(stack: MemoryStack): PointerBuffer {
        val glfwExtensions = glfwGetRequiredInstanceExtensions()!!
        if (ENABLE_VALIDATION_LAYERS) {
            stack.mallocPointer(glfwExtensions.capacity() + 1).apply {
                put(glfwExtensions)
                put(stack.UTF8(VK_EXT_DEBUG_UTILS_EXTENSION_NAME))
                return rewind()
            }
        }
        return glfwExtensions
    }

    private fun checkValidationLayerSupport(): Boolean {
        return useStack {
            val layerCount = ints(0)
            vkEnumerateInstanceLayerProperties(layerCount, null)

            val availableLayers = VkLayerProperties.calloc(!layerCount, this)
            vkEnumerateInstanceLayerProperties(layerCount, availableLayers)
            val availableLayerNames = availableLayers.map(VkLayerProperties::layerNameString).toSet()

            availableLayerNames.containsAll(validationLayers!!)
        }
    }

    private fun findQueueFamilies(device: VkPhysicalDevice): QueueFamilies {
        return useStack {
            val families = QueueFamilies()
            val pCount = ints(0)
            vkGetPhysicalDeviceQueueFamilyProperties(device, pCount, null)

            val properties = VkQueueFamilyProperties.calloc(!pCount, this)
            vkGetPhysicalDeviceQueueFamilyProperties(device, pCount, properties)
            val pSupport = mallocInt(1)
            for ((index, prop) in properties.withIndex()) {
                if (prop.queueCount() > 0 && prop.queueFlags() and VK_QUEUE_GRAPHICS_BIT != 0) {
                    families.graphics = index
                }
                vkGetPhysicalDeviceSurfaceSupportKHR(device, index, surface, pSupport).checkVKErrors()
                if (prop.queueCount() > 0 && !pSupport == VK_TRUE) {
                    families.presents = index
                }
                if (families.isComplete) break
            }
            families
        }
    }

    private fun isValidDevice(device: VkPhysicalDevice): Boolean {
        val indices = findQueueFamilies(device)
        val extensionsSupported = checkExtensions(device)
        var swapChainAdequate = false
        useStack {
            val swapChainSupport = querySwapChainSupport(device, this)
            swapChainAdequate = swapChainSupport.formats.hasRemaining() && swapChainSupport.presentModes.hasRemaining()
        }

        return indices.isComplete && extensionsSupported && swapChainAdequate
    }

    private fun chooseSwapSurfaceFormat(availableFormats: VkSurfaceFormatKHR.Buffer): VkSurfaceFormatKHR {
        return availableFormats.firstOrNull { it.format() == VK_FORMAT_B8G8R8A8_SRGB && it.colorSpace() == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR } ?: availableFormats.first()
    }

    private fun chooseSwapPresentMode(availablePresentMode: VkPresentModeKHR): Int {
        return (0 until availablePresentMode.capacity()).firstOrNull { it == VK_PRESENT_MODE_MAILBOX_KHR } ?: VK_PRESENT_MODE_FIFO_KHR
    }

    private fun chooseSwapExtent(capabilities: VkSurfaceCapabilitiesKHR, stack: MemoryStack): VkExtent2D {
        if (capabilities.currentExtent().width().toLong() != UInt.MAX_VALUE.toLong()) {
            return capabilities.currentExtent()
        }
        val actualExtent = VkExtent2D.calloc(stack).apply {
            set(RENDER_WIDTH, RENDER_HEIGHT)
            val minExtent2D = capabilities.minImageExtent()
            val maxExtent2D = capabilities.maxImageExtent()
            width(clamp(minExtent2D.width(), maxExtent2D.width(), width()))
            height(clamp(minExtent2D.height(), maxExtent2D.height(), height()))
        }
        return actualExtent
    }

    private fun querySwapChainSupport(device: VkPhysicalDevice, stack: MemoryStack): SwapchainSupportDetails {
        val details = SwapchainSupportDetails()
        details.capabilities = VkSurfaceCapabilitiesKHR.calloc(stack)
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, details.capabilities)

        val pCount = stack.ints(0)
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, pCount, null)
        details.formats = VkSurfaceFormatKHR.calloc(!pCount, stack)
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, pCount, details.formats)

        vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, pCount, null)
        details.presentModes = stack.mallocInt(!pCount)
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, pCount, details.presentModes)

        return details
    }

    override fun loop() {
        while (!glfwWindowShouldClose(windowPointer)) {
            glfwPollEvents()
            drawFrame()
        }
        vkDeviceWaitIdle(logicalDevice)
    }

    override fun cleanUp() {
        cleanUpSwapChain()

        for (i in 0 until MAX_FRAME_IN_FLIGHT) {
            vkDestroyBuffer(logicalDevice, uniformBuffers[i], allocator)
            vkFreeMemory(logicalDevice, uniformBuffersMemory[i], allocator)
        }

        vkDestroyDescriptorPool(logicalDevice, descriptorPool, allocator)

        vkDestroyDescriptorSetLayout(logicalDevice, descriptorSetLayout, allocator)

        vkDestroyBuffer(logicalDevice, indexBuffer, allocator)
        vkFreeMemory(logicalDevice, indexBufferMemory, allocator)

        vkDestroyBuffer(logicalDevice, vertexBuffer, allocator)
        vkFreeMemory(logicalDevice, vertexBufferMemory, allocator)

        for (i in 0 until MAX_FRAME_IN_FLIGHT) {
            vkDestroySemaphore(logicalDevice, imageAvailableSemaphores[i], allocator)
            vkDestroySemaphore(logicalDevice, renderFinishedSemaphores[i], allocator)
            vkDestroyFence(logicalDevice, inFlightFences[i], allocator)
        }
        vkDestroyCommandPool(logicalDevice, commandPool, allocator)

        vkDestroyPipeline(logicalDevice, graphicsPipeline, allocator)
        vkDestroyPipelineLayout(logicalDevice, pipelineLayout, allocator)
        vkDestroyRenderPass(logicalDevice, renderPass, allocator)

        vkDestroySurfaceKHR(vulkan, surface, allocator)
        vkDestroyDevice(logicalDevice, allocator)
        if (ENABLE_VALIDATION_LAYERS) {
            VulkanDebug.cleanUp(vulkan, allocator)
        }
        vkDestroyInstance(vulkan, allocator)
        glfwDestroyWindow(windowPointer)
        glfwTerminate()
    }

    private inline fun <T> useStack(action: MemoryStack.() -> T) = memoryStack.useStack(action)
}
