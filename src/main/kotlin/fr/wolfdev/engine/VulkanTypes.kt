package fr.wolfdev.engine

import java.nio.IntBuffer

typealias VkDebugUtilsMessengerExt = Long
typealias VkSurfaceKHR = Long
typealias VkSwapchainKHR = Long
typealias VkImage = Long
typealias VkImageView = Long
typealias VkShaderModule = Long
typealias VkRenderPass = Long
typealias VkDescriptorSetLayout = Long
typealias VkDescriptorPool = Long
typealias VkDescriptorSet = Long
typealias VkPipelineLayout = Long
typealias VkPipeline = Long
typealias VkFramebuffer = Long
typealias VkCommandPool = Long
typealias VkBuffer = Long
typealias VkDeviceMemory = Long
typealias VkDeviceSize = Long
typealias VkSemaphore = Long
typealias VkFence = Long

typealias VkFormat = Int
typealias VkMemoryPropertyFlags = Int
typealias VkBufferUsageFlags = Int

typealias VkPresentModeKHR = IntBuffer
