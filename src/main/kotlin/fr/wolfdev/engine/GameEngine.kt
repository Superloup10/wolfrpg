package fr.wolfdev.engine

import fr.wolfdev.engine.render.VulkanRenderingEngine
import org.lwjgl.glfw.GLFW.*

object GameEngine {
    private lateinit var game: Game
    private val time = glfwGetTime()

    fun launch(info: GameInformation, game: Game, renderDocUsed: Boolean) {
        this.game = game
        openNursery {
            var isStop = false
            start {
                VulkanRenderingEngine.init(info, renderDocUsed)
//                VulkanRenderingEngine.changeThread()
                VulkanRenderingEngine.loop()
                VulkanRenderingEngine.cleanUp()
                isStop = true
            }
            /*start {
                VulkanRenderingEngine.waitForInit()
                game.init()
                var lastStepTime = time
                while (!isStop) {
                    val dt = time - lastStepTime
                    game.tick(dt.toFloat())
                    lastStepTime = time
                    TimeUnit.MILLISECONDS.sleep(10L)
                }
            }*/
        }
    }
}
