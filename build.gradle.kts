import io.gitlab.arturbosch.detekt.Detekt
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.9.0"
    id("org.jlleitschuh.gradle.ktlint") version "11.5.1"
    id("io.gitlab.arturbosch.detekt") version "1.23.1"
    id("org.jetbrains.dokka") version "1.8.20"
    application
}

group = "fr.wolfdev"
version = "1.0-SNAPSHOT"
application.mainClass.set("$group.wolf.MainKt")

val lwjglNatives = Pair(
    System.getProperty("os.name")!!,
    System.getProperty("os.arch")!!
).let { (name, arch) ->
    when {
        arrayOf("Linux", "FreeBSD", "SunOS", "Unit").any { name.startsWith(it) } ->
            if (arrayOf("arm", "aarch64").any { arch.startsWith(it) }) {
                "natives-linux${if (arch.contains("64") || arch.startsWith("armv8")) "-arm64" else "-arm32"}"
            } else {
                "natives-linux"
            }

        arrayOf("Mac OS X", "Darwin").any { name.startsWith(it) } ->
            "natives-macos${if (arch.startsWith("aarch64")) "-arm64" else ""}"

        arrayOf("Windows").any { name.startsWith(it) } ->
            "natives-windows"

        else -> throw Error("Unrecognized or unsupported platform. Please set \"lwjglNatives\" manually")
    }
}

fun DependencyHandler.lwjgl(module: String, classifier: String = "") =
    create(group = "org.lwjgl", name = "lwjgl-$module", classifier = classifier)

repositories {
    mavenCentral()
    maven("https://oss.sonatype.org/content/repositories/snapshots/")
    maven("https://jitpack.io")
}

dependencies {
    implementation(kotlin("reflect"))
    implementation(platform("org.lwjgl:lwjgl-bom:3.3.2"))

    implementation("org.lwjgl:lwjgl")
    implementation(lwjgl("assimp"))
    implementation(lwjgl("glfw"))
    implementation(lwjgl("openal"))
    implementation(lwjgl("stb"))
    implementation(lwjgl("vulkan"))
    runtimeOnly("org.lwjgl", "lwjgl", classifier = lwjglNatives)
    runtimeOnly(lwjgl("assimp", classifier = lwjglNatives))
    runtimeOnly(lwjgl("glfw", classifier = lwjglNatives))
    runtimeOnly(lwjgl("openal", classifier = lwjglNatives))
    runtimeOnly(lwjgl("stb", classifier = lwjglNatives))
    if (lwjglNatives == "natives-macos" || lwjglNatives == "natives-macos-arm64") runtimeOnly(lwjgl("vulkan", classifier = lwjglNatives))
    implementation("org.joml:joml:1.10.5")

    implementation("com.github.jglrxavpok:JavaNursery:e80708550b")
    detektPlugins("io.gitlab.arturbosch.detekt:detekt-formatting:1.23.1")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "17"
    dependsOn(tasks.ktlintFormat)
}

ktlint {
    ignoreFailures.set(true)
    // additionalEditorconfigFile.set(file(".editorconfig"))
}

detekt {
    toolVersion = "1.23.1"
    source.from(files("src/main/kotlin"))
    config.from(files("detekt-config.yml"))
    buildUponDefaultConfig = true
}

tasks.withType<Detekt>().configureEach {
    reports {
        xml {
            outputLocation.set(file("$buildDir/reports/detekt/detekt.xml"))
            required.set(true)
        }
    }
}

tasks.dokkaHtml.configure {
    outputDirectory.set(buildDir.resolve("dokka"))
}
